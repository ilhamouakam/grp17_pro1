<p align="center"><a href="https://laravel.com" target="_blank"><img src="/public/erd_group17_sosmed.png" width="700"></a></p>



## Tentang Project - 1

Project pertama, ini adalah tentang sebuah Web Sosial Media, yang memilki:

- Membuat ERD
- Migrations
- Model + Eloquent
- Controller
- Laravel Auth + Middleware
- View (Blade)
- CRUD (Create Read Update Delete)
- Eloquent Relationships
- Laravel + Library/Packages



## Deskripsi singkat:

Aplikasi web untuk mengupdate status, profil, gambar, tulisan atau cerita.

# Terdapat Beberapa fitur utama:
- User harus terdaftar di web untuk menggunakan layanan sosmed
- User dapat membuat postingan berupa: tulisan, gambar dengan caption, quote.
- User dapat membuat, mengedit, dan menghapus pada postingan milik sendiri.
- Seorang User dapat mengikuti (follow) ke banyak User lainnya. Seorang User dapat diikuti oleh banyak User lainnya.
- Satu postingan dapat disukai(like) oleh banyak User. Satu User dapat menyukai (like) banyak Postingan.
- Satu postingan dapat memiliki banyak komentar. Satu komentar dapat dikomentari oleh banyak user.
- Satu komentar dapat disukai oleh banyak User. Satu User dapat menyukai banyak komentar.
- Seorang User dapat membuat dan mengubah profile nya sendiri.
- Pada halaman menampilkan postingan terdapat konten posting, komentar, jumlah komentar, jumlah like.
- Pada halaman profile User terdapat biodata, jumlah user pengikut(follower), jumlah user yang diikuti (following)


